<div class="popular-tabs">
    <ul class="nav-tab">
        <li class="active"><a data-toggle="tab" href="#tab-1">Thiết kế theo yêu cầu</a></li>
    </ul>
    <!--Tab 1--->
    <div class="tab-container">
        <div id="tab-1" class="tab-panel active">
            <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":3}}'>
                <?php foreach ($hot_pro as $pro) {// lay ra san pham ban chay
  //   print_r($pro);
                    ?>

                    <li>
                        <div class="left-block">
                            <a  href="<?php echo Yii::app()->request->baseUrl.'/Detail/Detail/id/'; ?><?php  echo $pro['id_product']?>"><img id="img_<?php echo $pro['id_product'] ?>" class="img-responsive" alt="product" src="<?php echo Yii::app()->request->baseUrl; ?>/<?php echo $pro['image'] ?>" /></a>
                            <div class="add-to-cart">
                                <button class="" type="submit" onclick="showAddCart('<?php echo $pro['id_product']?>')">Xem Nhanh</button>
                             
                            </div>
                        </div>
                        <div class="right-block">
                            <h5 class="product-name"><a id="name_<?php  echo $pro['id_product']?>" href="<?php echo Yii::app()->request->baseUrl.'/Detail/Detail/id/'; ?><?php  echo $pro['id_product']?>"><?php echo $pro['name'] ?></a></h5>
                            <div class="content_price">
                                <span class="price product-price" id="pri_<?php  echo $pro['id_product']?>"><?php echo $pro['price_new'] ?> VNĐ</span>
                            </div>
                            <div class="product-star">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <!--Tab 1--->

    </div>
</div>



<div class="container">
    <div class="row banner-bottom">
        <div class="col-sm-6">
            <div class="banner-boder-zoom">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/Category/ListProduct/id/203"><img alt="ads" class="img-responsive" src="<?php echo Yii::app()->request->baseUrl; ?>/data/ads17.jpg" /></a>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="banner-boder-zoom">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/Category/ListProduct/id/204"><img alt="ads" class="img-responsive" src="<?php echo Yii::app()->request->baseUrl; ?>/data/ads18.jpg" /></a>
            </div>
        </div>
    </div>
</div>